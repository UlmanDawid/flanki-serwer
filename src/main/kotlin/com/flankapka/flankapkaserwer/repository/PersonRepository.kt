package com.flankapka.flankapkaserwer.repository

import com.flankapka.flankapkaserwer.model.Person
import org.springframework.data.repository.CrudRepository

interface PersonRepository : CrudRepository<Person, Int>