package com.flankapka.flankapkaserwer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FlankapkaSerwerApplication

fun main(args: Array<String>) {
    runApplication<FlankapkaSerwerApplication>(*args)
}
