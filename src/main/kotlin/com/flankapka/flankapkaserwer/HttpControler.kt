package com.flankapka.flankapkaserwer

import com.flankapka.flankapkaserwer.model.Person
import com.flankapka.flankapkaserwer.repository.PersonRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class HttpControler (
        val personRepository: PersonRepository
){

    @GetMapping("/hello_world")
    fun helloWorld(): String {
        return "hello world"
    }

    @GetMapping("/all_people")
    fun getAllPeople() :MutableIterable<Person>{
        return personRepository.findAll()
    }

    @PostMapping("/person")
    fun addPerson(@RequestBody person: Person): Person{
        return personRepository.save(person)
    }
}